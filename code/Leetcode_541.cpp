#include <iostream>
#include <algorithm>
using namespace std;

class Solution {
public:
    string reverseStr(string s, int k) {
        int len = s.length();
        for(int i = 0; i < len; i += 2 * k){
            reverse(s.begin() + i, s.begin() + min(i+k, len));
        }
        return s;
    }
};


int main()
{
    
    system("pause");
    return 0;
}
