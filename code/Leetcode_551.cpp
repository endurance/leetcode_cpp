#include <iostream>

using namespace std;

class Solution {
public:
    bool checkRecord(string s) {
        int late = 0;
        int abcent = 0;
        for(int i = 0; i < s.length(); i++){
            if(s[i] == 'A'){
                ++abcent;
                if(abcent >= 2){
                    return false;
                }
            }
            if(s[i] == 'L'){
                ++late;
                if(late == 3){
                    return false;
                }
            }else{
                late = 0;
            }
        }
        return true;
    }
};

int main()
{
    
    system("pause");
    return 0;
}
