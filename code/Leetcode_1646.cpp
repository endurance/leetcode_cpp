#include <iostream>
#include <cmath>
#include<vector>
using namespace std;

class Solution {
public:
    int getMaximumGenerated(int n) {
        if(n < 2){
            return n;
        }
        vector<int> arr(n+1);
        arr[1] = 1;
        int maxNum = 1;
        for(int i = 2; i <= n; ++i){
            arr[i] = arr[i/2] + i % 2 * arr[i/2+1];
            maxNum = max(maxNum, arr[i]);
        }
        return maxNum;
    }
};


int main()
{
    system("pause");
    return 0;
}
