#include <iostream>
#include<vector>
#include<cmath>
using namespace std;

int compress(vector<char>& chars) {
    int length = chars.size();
    if(length <= 1){
        return length;
    }

    int write = 0;
    int count = 1;
    for(int i = 0; i < length; i++){
        char current = chars[i];
        char next = i == (length - 1) ? ' ' : chars[i+1];
        if(current != next){
            chars[write] = current;
            write++;
            if(count > 1){
                string countStr = to_string(count);
                int l = countStr.length();
                for(int j = pow(10, l-1); j > 0;){
                    chars[write] = (char)(count / j + 48);
                    write++;
                    count %= j;
                    j /= 10;
                }
            }
            count = 1;
        }else{
            count++;
        }
    }
    return write;
}

int main()
{
    
    system("pause");
    return 0;
}
