#include <iostream>
#include<set>
using namespace std;

class Solution {
public:

    bool isVowel(char ch){
        if(ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u'){
            return true;
        }
        if(ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U'){
            return true;
        }
        return false;
    }

    string reverseVowels(string s) {
    	int length = s.size();
    	int left = 0;
    	int right = length - 1;
    	while(left < right) {
    		while(left < length && !isVowel(s[left])) {
    			left++;
    		}
    		while(right > 0 && !isVowel(s[right])) {
    			right--;
    		}
    		if(left < right) {
    			char temp = s[left];
    			s[left] = s[right];
    			s[right] = temp;
    			left++;
    			right--;
    		}
    	}
    	return s;
    }
};


class Solution {
private:
   set<char> ss{'a','e','i','o','u','A','E','I','O','U'};
public:
    string reverseVowels(string s) {
        int i = 0;
        int j = s.size() - 1;
        while (i < j)
        {
            while (i < j && ss.find(s[i]) == ss.end())
            {
                ++i;
            }
            while (i < j && ss.find(s[j]) == ss.end())
            {
                --j;
            }
            if (i < j)
            {
                swap(s[i], s[j]);
                ++i;
                --j;
            }
        }

        return s;
    }
};


int main()
{
    
    system("pause");
    return 0;
}
