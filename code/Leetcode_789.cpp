#include <iostream>
#include <vector>
using namespace std;

bool escapeGhosts(vector<vector<int>>& ghosts, vector<int>& target) {
    int playerDistance = abs(target[0]) + abs(target[1]);
    for(int i = 0; i < ghosts.size(); i++){
        int ghostDistance = abs(ghosts[i][0] - target[0]) + abs(ghosts[i][1] - target[1]);
        if(ghostDistance <= playerDistance){
            return false;
        }
    }
    return true;
}

int main()
{
    
    system("pause");
    return 0;
}
